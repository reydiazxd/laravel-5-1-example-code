<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Mail\Message;

use Request;
use Password;
use Validator;
use JWTAuth;
use App\User;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail()
    {
        $validator = Validator::make(Request::all(), ['email' => 'required|email']);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $response = Password::sendResetLink(Request::only('email'), function (Message $message) {
            $message->subject("Tu enlace de recuperación de contraseña");
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return response()->json(['password_reset' => true], 202);

            case Password::INVALID_USER:
                return response()->json(['password_reset' => false], 400);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset()
    {
        $validator = Validator::make(Request::all(), [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $credentials = Request::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($u, $password) {
            $this->resetPassword($u, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                $user = User::whereEmail(Request::input('email'))->first();
                return response()->json(['password_reset' => true, 'access_token' => JWTAuth::fromUser($user)]);

            default:
                return response()->json(['password_reset' => false], 400);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);

        $user->save();

        return $user;
    }
}
