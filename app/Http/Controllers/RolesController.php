<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;

use App\Models\Role;
use App\User;


class RolesController extends Controller {

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    /*$this->middleware('jwt.auth',['except' => [
      'index',
      'show',
      'events'
    ]]);*/
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    /*$admin = new Role();
    $admin->name = "admin";
    $admin->display_name = "Avisado Admin";
    $admin->description = "User is the sysadmin";
    $admin->save();

    $staff = new Role();
    $staff->name = "staff";
    $staff->display_name = "Avisado Staff";
    $staff->description ="User is part of the Avisado Staff Team";
    $staff->save();

    $manager = new Role();
    $manager->name = "manager";
    $manager->display_name = "Manager";
    $manager->description ="User can manage and publish as an Organization";
    $manager->save();

    $user = new Role();
    $user->name = "user";
    $user->display_name ="User";
    $user->description = "A user of the platform";
    $user->save();*/

    return Role::findOrFail(1)->users()->get();
    $user = User::find(1);
    //$user->roles()->attach(1);
    return $user->roles()->get();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
  }

}
