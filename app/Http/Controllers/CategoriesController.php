<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;

use App\Models\Category;

class CategoriesController extends Controller {

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('jwt.auth',['except' => [
      'index',
      'show',
      'events'
    ]]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    return Category::all();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    return Category::findOrFail($id);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $category = Category::findOrFail($id);
    $category->name = Request::input('name');
    $category->description = Request::input('description');
    $category->save();
    return $category;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    //Create and set the values on the object
    $category = new Category(Request::only('name', 'description'));
    $category->save();
    return response($category, 201);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    try {
      $category = Category::findOrFail($id);
      if($category->destroy()) {
        return response('', 204);
      } else {
        return response(['status' => "Couldn't delete content"], 400);
      }
    } catch (Exception $e) {
      return response($e, 400);
    }
  }

  public function events($id) {
    if($category = Category::find($id)) {
      return $category->events()->get();
    }
    return response('', 404);
  }

}
