<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We load the roles to assign their permissions
        $admin = Role::whereName('admin');
        $staff = Role::whereName('staff');
        $manager = Role::whereName('manager');
        $user = Role::whereName('user');


        $createCategory = new Permission();
        $createCategory->name = 'create-category';
        $createCategory->display_name = 'Create Categories';
        $createCategory->description = 'Allows a user to create new categories';
        $createCategory->save();
    }
}
