<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
          'firstname' => 'Reinaldo',
          'lastname' => 'Díaz',
          'email' => 'reinaldo122@gmail.com',
          'phone' => '+584120576001',
          'birth_date' => date('c'),
          'password' => Hash::make('password'),
          'confirmed' => 0
        ]);
    }
}
