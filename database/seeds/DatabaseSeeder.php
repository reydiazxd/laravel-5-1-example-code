<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Models\Role;
use App\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);

        $user = User::find(1);
        $user->roles()->attach(1);

        

        

        //$this->call(CategoriesTableSeeder::class);
        //factory(App\Models\Location::class, 50)->create();
        //factory(App\Models\Event::class, 150)->create();
        //$this->call(LocationsTableSeeder::class);
        //$this->call(LocationsTableSeeder::class);

        Model::reguard();
    }
}
